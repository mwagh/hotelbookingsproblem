﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HotelBookingsProblem.Models;

namespace HotelBookingsProblem.Data
{
    public class HotelRoomRepository : IHotelRoomRepository
    {
        private readonly IEnumerable<IHotelRoom> _hotelRooms;

        public HotelRoomRepository(IEnumerable<IHotelRoom> hotelRooms)
        {
            _hotelRooms = hotelRooms;
        }

        public bool GetRoomAvailability(int roomNumber, DateTime bookingDate)
        {
            var room = GetRoomByNumber(roomNumber);
            return room.IsAvailableOn(bookingDate);
        }

        public void BookRoom(string guest, int roomNumber, DateTime bookingDate)
        {
            var room = GetRoomByNumber(roomNumber);
            room.BookFor(guest, bookingDate);
        }

        public List<int> GetAvailableRooms(DateTime bookingDate)
        {
            return _hotelRooms
                .Where(room => room.IsAvailableOn(bookingDate))
                .Select(room => room.RoomNumber)
                .ToList();
        }

        private IHotelRoom GetRoomByNumber(int roomNumber)
        {
            var room = _hotelRooms.FirstOrDefault(x => x.RoomNumber == roomNumber);
            if (room == null)
            {
                throw new ArgumentException($"Invalid room number: {roomNumber}");
            }

            return room;
        }
    }
}