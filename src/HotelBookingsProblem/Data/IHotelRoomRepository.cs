using System;
using System.Collections;
using System.Collections.Generic;

namespace HotelBookingsProblem.Data
{
    public interface IHotelRoomRepository
    {
        bool GetRoomAvailability(int roomNumber, DateTime bookingDate);
        void BookRoom(string guest, int roomNumber, DateTime bookingDate);
        List<int> GetAvailableRooms(DateTime bookingDate);
    }
}