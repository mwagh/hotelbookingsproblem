﻿using System;
using System.Collections.Generic;
using HotelBookingsProblem.Data;

namespace HotelBookingsProblem.Models
{
    public class BookingManager: IBookingManager
    {
        private IHotelRoomRepository _repository;

        public BookingManager(IHotelRoomRepository repository)
        {
            _repository = repository;
        }
        public bool IsRoomAvailable(int room, DateTime date)
        {
            return _repository.GetRoomAvailability(room, date);
        }

        public void AddBooking(string guest, int room, DateTime date)
        {
            _repository.BookRoom(guest, room, date);
        }

        public List<int> GetAvailableRooms(DateTime date)
        {
            return _repository.GetAvailableRooms(date);
        }
    }
}