﻿using System;
using System.Collections;
using System.Collections.Generic;
using HotelBookingsProblem.Data;
using HotelBookingsProblem.Models;
using Microsoft.Extensions.DependencyInjection;

namespace HotelBookingsProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            services.AddSingleton<IEnumerable<IHotelRoom>>(x => new List<IHotelRoom>()
            {
                new HotelRoom(101),
                new HotelRoom(102),
                new HotelRoom(201),
                new HotelRoom(203)
            });
            
            services.AddSingleton<IHotelRoomRepository, HotelRoomRepository>();
            var serviceProvider = services.BuildServiceProvider();

            IBookingManager bm = new BookingManager(serviceProvider.GetRequiredService<IHotelRoomRepository>());
            var today = new DateTime(2012,3,28);
            Console.WriteLine(string.Join(",", bm.GetAvailableRooms(today))); // outputs 101, 102, 201, 203
            Console.WriteLine(bm.IsRoomAvailable(101, today)); // outputs true 
            bm.AddBooking("Patel", 101, today);
            Console.WriteLine(string.Join(",", bm.GetAvailableRooms(today))); // outputs 102, 201, 203
            Console.WriteLine(bm.IsRoomAvailable(101, today)); // outputs false 
            bm.AddBooking("Li", 101, today); // throws an exception
        }
    }
}