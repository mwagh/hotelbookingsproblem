﻿using System;
using System.Collections.Concurrent;

namespace HotelBookingsProblem.Models
{
    public class HotelRoom : IHotelRoom
    {
        public int RoomNumber { get; }
        private readonly ConcurrentDictionary<DateTime, string> _bookings;

        public HotelRoom(int roomNumber)
        {
            RoomNumber = roomNumber;
            _bookings = new ConcurrentDictionary<DateTime, string>();
        }

        public bool IsAvailableOn(DateTime bookingDate)
        {
            return !_bookings.ContainsKey(bookingDate);
        }
        
        public void BookFor(string guest, DateTime bookingDate)
        {
            if (!_bookings.TryAdd(bookingDate, guest))
            {
                throw new InvalidOperationException($"Attempted to book unavailable room: {RoomNumber}");
            }
        }
    }
}