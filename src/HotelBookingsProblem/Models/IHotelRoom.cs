﻿using System;

namespace HotelBookingsProblem.Models
{
    public interface IHotelRoom
    {
        int RoomNumber { get; }
        bool IsAvailableOn(DateTime bookingDate);
        void BookFor(string guest, DateTime bookingDate);
    }
}