using System;
using System.Collections.Generic;
using HotelBookingsProblem.Data;
using HotelBookingsProblem.Models;
using NSubstitute;
using Xunit;

namespace HotelBookingsProblemTests
{
    public class BookingManagerTests
    {
        private readonly IHotelRoomRepository _hotelRoomRepository;
        private readonly BookingManager _subject;


        public BookingManagerTests()
        {
            _hotelRoomRepository = Substitute.For<IHotelRoomRepository>();
            _subject = new BookingManager(_hotelRoomRepository);
        }

        #region IsRoomAvailable
        [Fact]
        public void IsRoomAvailable_WithAvailableRoom_ReturnsTrue()
        {
            var availableRoomNumber = 101;
            _hotelRoomRepository
                .GetRoomAvailability(Arg.Any<int>(), Arg.Any<DateTime>())
                .Returns(true);

            var isRoomAvailable = _subject.IsRoomAvailable(availableRoomNumber, DateTime.Today);
            
            Assert.True(isRoomAvailable);
        }
        
        [Fact]
        public void IsRoomAvailable_WithUnavailableRoom_ReturnsFalse()
        {
            var unvailableRoomNumber = 102;
            _hotelRoomRepository
                .GetRoomAvailability(Arg.Any<int>(), Arg.Any<DateTime>())
                .Returns(false);
            
            var isRoomAvailable = _subject.IsRoomAvailable(unvailableRoomNumber, DateTime.Today);
            
            Assert.False(isRoomAvailable);
        }
        #endregion

        #region AddBooking
        [Fact]
        public void AddBooking_WhenRoomIsAvailable_Succeeds()
        {
            var roomToBook = 101;
            _hotelRoomRepository
                .GetRoomAvailability(Arg.Any<int>(), Arg.Any<DateTime>())
                .Returns(true);
            
            _subject.AddBooking("test1", roomToBook, DateTime.Today);
        }
        
        [Fact]
        public void AddBooking_WhenRoomIsDoubleBooked_Throws()
        {
            var roomToBook = 101;
            var bookingDate = DateTime.Today;
            _hotelRoomRepository
                .GetRoomAvailability(Arg.Any<int>(), Arg.Any<DateTime>())
                .Returns(true);
            
            _subject.AddBooking("test1", roomToBook, bookingDate);

            var exception = Assert.Throws<Exception>(() => _subject.AddBooking("test2", roomToBook, bookingDate));
            Assert.Equal($"Attempted to book unavailable room: {roomToBook}", exception.Message);
        }
        #endregion

        #region GetAvailableRooms

        [Fact]
        public void GetAvailableRooms_WithNoBookings_ReturnsAllRooms()
        {
            var expectedRooms = new List<int> {101, 102, 201, 203};
            var bookingDate = DateTime.Today;
            _hotelRoomRepository
                .GetAvailableRooms(Arg.Is(bookingDate))
                .Returns(expectedRooms);
            
            var availableRooms = _subject.GetAvailableRooms(bookingDate);
            
            Assert.Equal(expectedRooms, availableRooms);
        }
        
        #endregion
    }
}