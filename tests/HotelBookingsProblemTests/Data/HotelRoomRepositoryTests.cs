﻿using System;
using System.Collections.Generic;
using System.Linq;
using HotelBookingsProblem.Data;
using HotelBookingsProblem.Models;
using NSubstitute.Core;
using Xunit;

namespace HotelBookingsProblemTests
{
    public class HotelRoomRepositoryTests
    {
        private readonly HotelRoomRepository _subject;
        
        public HotelRoomRepositoryTests()
        {
            var hotelRooms = new List<IHotelRoom>()
            {
                new HotelRoom(101),
                new HotelRoom(102),
                new HotelRoom(201),
                new HotelRoom(203)
            };
            _subject = new HotelRoomRepository(hotelRooms);
        }

        #region GetRoomAvailability
        [Fact]
        public void GetRoomAvailability_WhenRoomIsAvailable_ReturnsTrue()
        {
            var isAvailable = _subject.GetRoomAvailability(101, DateTime.Today);
            
            Assert.True(isAvailable);
        }

        [Fact]
        public void GetRoomAvailability_WithInvalidRoomNumber_ThrowsArgumentException()
        {
            var invalidRoomNumber = -1;
            var exception = Assert.Throws<ArgumentException>(() => _subject.GetRoomAvailability(invalidRoomNumber, DateTime.Today));
            Assert.Equal($"Invalid room number: {invalidRoomNumber}", exception.Message);
        }
        #endregion

        #region BookRoom
        [Fact]
        public void BookRoom_WhenRoomIsAvailable_Succeeds()
        {
            var roomToBook = 101;
            var bookingDate = DateTime.Today;
            
            _subject.BookRoom("test1", roomToBook, bookingDate);
            var isAvailable = _subject.GetRoomAvailability(roomToBook, bookingDate);
            
            Assert.False(isAvailable);
        }
        
        [Fact]
        public void BookRoom_WithInvalidRoomNumber_ThrowsArgumentException()
        {
            var invalidRoomNumber = -1;
            var bookingDate = DateTime.Today;
            
            var exception = Assert.Throws<ArgumentException>(() => _subject.BookRoom("test1", invalidRoomNumber, bookingDate));
            Assert.Equal($"Invalid room number: {invalidRoomNumber}", exception.Message);
        }
        
        [Fact]
        public void BookRoom_WhenRoomIsDoubleBooked_Throws()
        {
            var roomToBook = 101;
            var bookingDate = DateTime.Today;
            
            _subject.BookRoom("test1", roomToBook, bookingDate);
            
            var isAvailable = _subject.GetRoomAvailability(roomToBook, bookingDate);
            Assert.False(isAvailable);
            var exception = Assert.Throws<InvalidOperationException>(() => _subject.BookRoom("test1", roomToBook, bookingDate));
            Assert.Equal($"Attempted to book unavailable room: {roomToBook}", exception.Message);
        }
        
        [Fact]
        public void BookRoom_WhenSameRoomIsBookedForNewDate_Succeeds()
        {
            var roomToBook = 101;
            var bookingDate = DateTime.Today;
            var laterBookingDate = bookingDate.AddDays(1);
            
            _subject.BookRoom("test1", roomToBook, bookingDate);
            _subject.BookRoom("test1", roomToBook, laterBookingDate);
            
            var isFirstDateAvailable = _subject.GetRoomAvailability(roomToBook, bookingDate);
            var isSecondDateAvailable = _subject.GetRoomAvailability(roomToBook, laterBookingDate);
            Assert.False(isFirstDateAvailable);
            Assert.False(isSecondDateAvailable);
        }
        #endregion

        #region GetAvailableRooms

        [Fact]
        public void GetAvailableRooms_WithNoExistingBookings_ReturnsAllRooms()
        {
            var expectedRooms = new List<int> {101, 102, 201, 203};
            var bookingDate = DateTime.Today;
            
            var availableRooms = _subject.GetAvailableRooms(bookingDate);
            
            Assert.Equal(expectedRooms ,availableRooms);
        }

        [Fact]
        public void GetAvailableRooms_WithExistingBooking_ReturnsAvailableRooms()
        {
            var roomToBook = 101;
            var bookingDate = DateTime.Today;
            
            _subject.BookRoom("test1", roomToBook, bookingDate);
            var availableRooms = _subject.GetAvailableRooms(bookingDate);
            
            Assert.DoesNotContain(roomToBook, availableRooms);
        }
        #endregion
    }
}